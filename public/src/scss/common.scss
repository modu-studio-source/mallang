@charset "UTF-8";
@import "variables";
@import "mixins";

@font-face {
    font-family: 'notokr';
    src: url('../fonts/notokr-thin.eot');
    src: url('../fonts/notokr-thin.eot?#iefix') format('embedded-opentype'),
         url('../fonts/notokr-thin.woff2') format('woff2'),
         url('../fonts/notokr-thin.woff') format('woff'),
         url('../fonts/notokr-thin.ttf') format('truetype'),
         url('../fonts/notokr-thin.svg#notokr-thin') format('svg');
    font-weight: 100;
    font-style: normal;
}
@font-face {
    font-family: 'notokr';
    src: url('../fonts/notokr-light.eot');
    src: url('../fonts/notokr-light.eot?#iefix') format('embedded-opentype'),
         url('../fonts/notokr-light.woff2') format('woff2'),
         url('../fonts/notokr-light.woff') format('woff'),
         url('../fonts/notokr-light.ttf') format('truetype'),
         url('../fonts/notokr-light.svg#notokr-light') format('svg');
    font-weight: 200;
    font-style: normal;
}

@font-face {
    font-family: 'notokr';
    src: url('../fonts/notokr-demilight.eot');
    src: url('../fonts/notokr-demilight.eot?#iefix') format('embedded-opentype'),
         url('../fonts/notokr-demilight.woff2') format('woff2'),
         url('../fonts/notokr-demilight.woff') format('woff'),
         url('../fonts/notokr-demilight.ttf') format('truetype'),
         url('../fonts/notokr-demilight.svg#notokr-demilight') format('svg');
    font-weight: 300;
    font-style: normal;
}

@font-face {
    font-family: 'notokr';
    src: url('../fonts/notokr-regular.eot');
    src: url('../fonts/notokr-regular.eot?#iefix') format('embedded-opentype'),
         url('../fonts/notokr-regular.woff2') format('woff2'),
         url('../fonts/notokr-regular.woff') format('woff'),
         url('../fonts/notokr-regular.ttf') format('truetype'),
         url('../fonts/notokr-regular.svg#notokr-regular') format('svg');
    font-weight: 400;
    font-style: normal;
}

@font-face {
    font-family: 'notokr';
    src: url('../fonts/notokr-medium.eot');
    src: url('../fonts/notokr-medium.eot?#iefix') format('embedded-opentype'),
         url('../fonts/notokr-medium.woff2') format('woff2'),
         url('../fonts/notokr-medium.woff') format('woff'),
         url('../fonts/notokr-medium.ttf') format('truetype'),
         url('../fonts/notokr-medium.svg#notokr-medium') format('svg');
    font-weight: 500;
    font-style: normal;
}
@font-face {
    font-family: 'notokr';
    src: url('../fonts/notokr-bold.eot');
    src: url('../fonts/notokr-bold.eot?#iefix') format('embedded-opentype'),
         url('../fonts/notokr-bold.woff2') format('woff2'),
         url('../fonts/notokr-bold.woff') format('woff'),
         url('../fonts/notokr-bold.ttf') format('truetype'),
         url('../fonts/notokr-bold.svg#notokr-bold') format('svg');
    font-weight: 600;
    font-style: normal;
}
@font-face {
    font-family: 'notokr';
    src: url('../fonts/notokr-black.eot');
    src: url('../fonts/notokr-black.eot?#iefix') format('embedded-opentype'),
         url('../fonts/notokr-black.woff2') format('woff2'),
         url('../fonts/notokr-black.woff') format('woff'),
         url('../fonts/notokr-black.ttf') format('truetype'),
         url('../fonts/notokr-black.svg#notokr-black') format('svg');
    font-weight: 700;
    font-style: normal;
}


// Style
//
// *reset.css*가 아닌 *normalize.css* ( https://necolas.github.io/normalize.css/ )를 사용합니다.
//
// Styleguide 1.

fieldset { border:none; margin:0; padding:0; }
legend { position:absolute; top:-999em; left:-999em; }
b,
strong { font-weight:bold; }
a { color:inherit; text-decoration:none; }
p { line-height:1.5; margin:1em 0;}

input:not([type='checkbox']),
input:not([type='radio']),
textarea,
select { border-radius:0;}

input:not([type='checkbox']),
input:not([type='radio']),
textarea,
select { appearance: none; }

button,
input[type='checkbox'],
input[type='radio'],
input[type='submit'],
input[type='image'] { cursor:pointer; }

// Font
//
// Styleguide 1.1

// Font Family
//
// * `Nanum Gothic` :기본 글꼴
// * `Opan Sans` : 보조 글꼴
//
// Markup:
// <span class="font1">가나다라마바사아자차카타파하 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 123456789</span><br>
// <span class="font2">가나다라마바사아자차카타파하 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 123456789</span>
//
// Styleguide 1.1.1
.font1 { font-family:$font1;}
.font2 { font-family:$font2;}

// Color
//
// Styleguide 1.2

// Gray Color
//
// * `#767676` : 본문 기본
// * `#222222` : 강조 및 제목
//
// Markup:
// <span class="gray1">가나다라마바사아자차카타파하 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 123456789</span><br>
// <span class="gray2">가나다라마바사아자차카타파하 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 123456789</span>
//
// Styleguide 1.2.1
.gray1 { color:$gray1; }
.gray2 { color:$gray2; }

// Point Color
//
// * `#1991d7`
//
// Markup:
// <span class="color1">가나다라마바사아자차카타파하 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 123456789</span>
//
// Styleguide 1.2.1
.color1 { color:#fff !important;}
.color2 { color:#009e52;}
.color3 { color:#f24443;}
.color4 { color:#000 !important;}
.color5 { color:#00a39f !important;}
.color6 { color:#7ebbff !important;}
.color7 { color:#4177bb !important;}
.color8 { color:#010000 !important;}
.h180{height:180px !important;}
.h200{height:200px !important;}
.mb50{margin-bottom:50px !important;}
// Common Class
//
// Styleguide 1.3

// hidden
//
// * `class="hidden"` : 화면 숨김
//
// Markup:
// <span class="hidden">가나다라마바사아자차카타파하 ABCDEFGHIJKLMNOPQRSTUVWXYZ abcdefghijklmnopqrstuvwxyz 123456789</span>
//
// Styleguide 1.3.1
.hidden { @include hidden; }
/*모바일시 줄바꿈*/
.mobile{display:none;
	@media only screen and (max-width:$tablet-size) {display:block;}
}
.hidden-m{
	@media only screen and (max-width:$tablet-size) {display:none !important;}
}
/*게시판*/
.board-magazine-v1 { margin:0; padding:0; list-style:none; border-top:2px solid #8c8a8c; border-bottom:1px solid #8c8a8c; line-height:1.5;
	&__item {position:relative; border-top:1px solid #dedfde;
		&:first-child { border-top:none; }
	}
	&__link { overflow:hidden; display:block; padding:20px 0; color:$black-body; text-decoration:none;
		&:hover {

		}
	}
	&__img-wrap { overflow:hidden;float:left; position:relative; width:200px; height:100px; border:1px solid #dedfde; margin-right:35px; }
	&__txt-wrap { overflow:hidden; position:relative; }
	&__img { position:absolute; top:0; left:0; right:0; bottom:0; margin:auto; max-width:100%; }
	&__tit { display:table-cell; padding-bottom:15px; font-size:18px; color:$black-tit; font-weight:500; }
	&__edge { display:table-cell; width:1%; padding-left:10px; color:#999; text-align:right; }
	&__summary {position:static; display: -webkit-box; overflow: hidden; -webkit-box-orient: vertical; clear:both; height:3em; -webkit-line-clamp:2; line-height:1.5em; text-overflow: ellipsis; word-wrap:break-word; font-size:16px; color:#666; }
	@media only screen and (max-width:$tablet-size) { line-height:1.3;
		&__link { padding-top:15px; padding-bottom:15px; }
		&__img-wrap { width:100px; height:50px; margin-right:10px; }
		&__txt-wrap {
			&:after { display:none; }
		}
		&__tit { overflow:hidden; display: -webkit-box; height:20px; margin-bottom:10px; padding-bottom:0; line-height:20px; -webkit-line-clamp:1; -webkit-box-orient: vertical; position:relative; font-size:14px; text-overflow: ellipsis; word-wrap:break-word; }
		&__edge { display:block; position:absolute; bottom:0; left:0; width:auto; padding-left:0; font-size:12px; }
		&__summary {padding-right:0; font-size:12px; height:4.5em; -webkit-line-clamp:3;}
	}
}

/*게시판 보기*/
.board-view{border-top:2px solid #363636; padding:40px 0 70px; border-bottom:1px solid #ededed;
	&__tit{margin:0 0 40px; font-size:30px; color:#000000; font-weight:500;}
	&__box{text-align:center;}
	&__txt{margin:0; text-align:left; color:#666; font-size:16px; line-height:1.6;}
	@media only screen and (max-width:$tablet-size) {border-top:none; padding:0 0 20px;
		&__tit{margin:0 0 20px; font-size:17px;}
		&__img{width:100%;}
		&__txt{font-size:13px;}
	}
}
/*페이징*/
.pagination-v1 { overflow:hidden; position:relative; margin-top:40px; text-align:center; font-size:0;
	&__info { display:none; line-height:34px; color:#999; font-size:13px; }
	&__location { color:$black-tit;}
	&__group { display:inline-block; text-align:center; vertical-align:middle; }
	&__item { -webkit-box-sizing:border-box; box-sizing:border-box; display:inline-block; min-width:34px; height:34px; margin:0 4px; border:1px solid #e6e6e6; font-size:13px; color:#999; text-decoration:none; line-height:32px; border-radius: 7%;
		&.on { border-color:#dbdbdb; background-color:#363636; color:#fff; }
		@mixin ico ($position) { overflow:hidden; position:relative; width:34px; text-indent:-1000em;
			&:before { position:absolute; top:0; left:0; right:0; bottom:0; width:11px; height:9px; margin:auto; @include background-image-retina('../images/board/pagination-v1-ico','png',22px,18px); background-repeat:no-repeat; background-position:$position; content:''; }
		}
		&_home { @include ico(0 0) }
		&_end { @include ico(100% 100%) }
		&_before { display:none; @include ico(100% 0) }
		&_after { display:none; @include ico(0 100%) }
		&_prev { @include ico(100% 0) }
		&_next { @include ico(0 100%) }
	}
	@media only screen and (max-width:$tablet-size) {
		margin-top:20px;
		&__info { position:absolute; top:0; left:0; right:0; display:block; }
		&__group { display:none; position:relative; z-index:10;
			&_prev { display:block; float:left; margin-left:-4px; }
			&_next { display:block; float:right; margin-right:-4px; }
		}
		&__item { -webkit-box-sizing:border-box; box-sizing:border-box; display:inline-block; min-width:34px; height:34px; margin:0 4px; border:1px solid #e6e6e6; font-size:12px; color:#999; text-decoration:none; line-height:32px;
			&_before,
			&_after { display:inline-block; }
			&_prev,
			&_next { display:none; }
		}
	}
}

/*버튼*/
.btn-area{position:relative; margin-top:36px; text-align:center;
	&:after{display:block; clear:both; content:'';}
	&__prev{float:left; display:block;
		& .btn-area__wrap{display:inline-block; height:40px; line-height:40px;
			&:before{display:inline-block; width:8px; height:40px; margin-right:5px; background:url('../images/common/board-view_prev.png') no-repeat center 12px; vertical-align:middle;}
		}
	}
	&__center{position:absolute; top:0; left:0; right:0; display:inline-block;}
	&__list{
		& .btn-area__wrap{display:inline-block; height:40px; line-height:40px;
			&:before{display:inline-block; width:14px; height:40px; margin-right:5px; background:url('../images/common/board-view_list.png') no-repeat center 12px; vertical-align:middle;}
		}
	}
	&__next{float:right; display:block;
		& .btn-area__wrap{display:inline-block; height:40px; line-height:40px;
			&:after{display:inline-block; width:8px; height:40px; margin-left:5px; background:url('../images/common/board-view_next.png') no-repeat center 12px; vertical-align:middle; content:'';}
		}
	}
	&__wrap{position:relative; font-size:14px; color:#000; box-sizing:border-box;
		&:before{content:'';}
	}
	@media only screen and (max-width:$tablet-size) {margin-top:23px;
		&__wrap{font-size:12px;}
		&__prev{float:left; display:block;
			& .btn-area__wrap{
				&:before{width:5px; background:url('../images/common/board-view_prev_m.png') no-repeat center 14px; background-size:5px 9px;}
			}
		}
		&__center{position:absolute; top:0; left:0; right:0; display:inline-block;}
		&__list{
			& .btn-area__wrap{
				&:before{ width:10px; background:url('../images/common/board-view_list_m.png') no-repeat center 14px; background-size:10px 8px;}
			}
		}
		&__next{float:right; display:block;
			& .btn-area__wrap{
				&:after{width:5px; background:url('../images/common/board-view_next_m.png') no-repeat center 14px; background-size:5px 9px;}
			}
		}
	}
}

/*탭*/
.tab-v1 {width:146px; position:absolute; top:22px; right:0; z-index:600;
	&.type-another{display:none; top:36px; width:auto;
		& .tab-v1__list{display:block;}
		& .tab-v1__link{width:auto; padding:0px 15px;}
		& .tab-v1__item{float:left; display:inline-block; width:auto; flex-grow:0;}
	}
	$this:&;
	$item:#{$this}__item;
	&__list { display:flex; /*border-collapse:collapse;*/ padding:0; margin:0;}
	&__item {position:relative; box-sizing:border-box; display:table-cell; flex-grow:1; width:1%;  color:$black-body; font-size:16px; text-align:center; vertical-align:middle;
		&:first-child{
			& .tab-v1__link:before{border-left:none;}
		}
		&.is-active { border-top-color:#fff; border-bottom-color:#fff; background-color:#fff; }
	}
	&__link {height:100%; box-sizing:border-box; display:table-cell; width:1000px; padding:0px 20px; height:inherit; color:inherit; text-decoration:none;  vertical-align:middle; box-sizing:border-box;
		#{$item}.is-active & { border-top:none; color:#4d2ee6; font-weight:$regular;}
		&:before {position:absolute; left:0; top:0; bottom:0; height:14px; margin:auto; border-left:1px solid #cccccc; content:'';}
	}
	&__m-tit{display:none;}
	@media only screen and (max-width:$tablet-size) {
		position:fixed; width:auto; top:44px; left:0; height:70px;
		&.type-another{display:block; top:44px;}
		&.type-white{
			& .tab-v1__list{
				&:before { box-sizing:border-box; position:absolute; z-index:100; top:27px; right:15px; width:10px; height:10px; border-width:0 0 1px 1px; border-style:solid; border-color:transparent transparent #fff #fff; content:''; transform:rotate(-45deg); }
			}
			& .tab-v1__m-tit{color:#fff;}
		}
		&.js-white{
			& .tab-v1__list{
				&:before { box-sizing:border-box; position:absolute; z-index:100; top:27px; right:15px; width:10px; height:10px; border-width:0 0 1px 1px; border-style:solid; border-color:transparent transparent #fff #fff; content:''; transform:rotate(-45deg); }
			}
		}
		&__list { display:block; overflow:hidden; position:absolute; z-index:500; width:100%; padding-top:70px; max-height:0;
			&.is-active{background-color:rgba(255,255,255,0.9); border-bottom:1px solid #cfcfcf;}
			&:before { box-sizing:border-box; position:absolute; z-index:100; top:27px; right:15px; width:10px; height:10px; border-width:0 0 1px 1px; border-style:solid; border-color:transparent transparent #363636 #363636; content:''; transform:rotate(-45deg); }
			//&:after { z-index:400; position:absolute; top:0; left:0; right:0; height:40px; content:''; }
			&.js-open-m { max-height: 1000px; background-color:#fff !important;
				&:before{transform:rotate(135deg);}
			}
		}
		&__item {position:static; display:block; width:auto; height:45px; padding:0px 40px; font-size:13px; text-align:left; font-weight:$normal;
			&:last-child{margin-bottom:20px; border-bottom:none;
				& .tab-v1__link{border-bottom:none;}
			}
		}
		&__link { position:static; padding:10px 0px; border-bottom:1px solid #d9d9d9;
			#{$item}.is-active & {font-weight:$normal;
				// &:before { display:block; box-sizing:border-box; position:absolute; z-index:10; top:0; left:0; width:100%; border-left:none; margin:0; padding:23px 16px; content:attr(data-title); font-weight:$bold; color:#333333; font-size:18px; }
				&:after { display:none; }
			}
			&:before{display:none;}
		}
		&.type-another{
			& .tab-v1__item{float:none; display:block;}
			& .tab-v1__link{display:table-cell; width:1%; padding:10px 0px;}
		}
		&__m-tit{display:block; box-sizing:border-box; position:absolute; z-index:500; top:0; left:0; width:100%; border-left:none; margin:0; padding:23px 16px; font-weight:600; color:#010000; font-size:24px; cursor:pointer;
			&.type-another{color:#fff;}
		}
	}
}

.tab-content {
	display:none;
	@media only screen and (max-width:$tablet-size) {
		padding-top:70px;
		&.type-another{padding-top:0;}
	}
	&.is-active { display:block; }
}
/*묶음*/
.wrap{position:relative; width:1240px; margin:0px auto; height:100%;
	@media only screen and (max-width:$tablet-size) {width:100%; padding:0px 15px; box-sizing:border-box;}
	@media only screen and (max-width:$pro-size) {overflow:hidden; width:1024px; padding:0px 20px; box-sizing:border-box;}
}

.responsiveImg{width:100%;}

/*비디오*/
.video{display:none; position:fixed; top:0; left:0; bottom:0; right:0; text-align:center; overflow:auto; z-index:9000; background-color:#000; background-color:rgba(0,0,0,0.7);
	&__vertical{opacity:0; display:inline-block; max-width:900px; width:90%; box-sizing:border-box;  vertical-align:middle; transition: all 0.5s;-webkit-transition: all 0.5s;
		&.is-active{opacity:1;}
	}
	&__box{position:relative; width:100%; height:0;  padding-bottom:55.55%; }
	&__wrap{position:absolute; top:0; left:0; width:100%; height:100%;}
	&:after{display:inline-block; height:100%; vertical-align: middle; content:'';}
	&__close{position:absolute; top:-30px; right:0; overflow:hidden; display:inline-block; width:16px; height:16px; background:url('../images/main/video_close.png'); z-index:9500; text-indent:-9999px;}
}

/*인비게이터*/
.page{position:absolute; display:block; width:100%; text-align:center; bottom:60px;
	&__list{display:inline-block;
		& .bx-pager-item{position:relative; overflow:hidden; display:inline-block;  text-indent:-999px; width:12px; height:12px;
			& a{position:absolute; top:0; left:0; width:8px; height:8px;  background-color:#ccc; border:1px solid #ccc; border-radius:50%;
				&.active{background-color:#4d2ee6; border:1px solid #4d2ee6;}
			}
			& + .bx-pager-item{margin-left:12px;}
		}
	}
	@media only screen and (max-width:$tablet-size) {bottom:30px;}
	&__start{display:inline-block; margin-left:12px;
		& a{overflow:hidden; display:inline-block; width:7px; height:12px; text-indent:-9999px; font-size:14px;}
		& .bx-stop{background:url('../images/main/pause_btn.png') no-repeat; }
		& .bx-start{background:url('../images/main/start_btn.png') no-repeat;}
	}
}
strong{font-weight:600;}
.normal{font-weight:400;}
.medium{font-weight:500 !important;}

.table-v1{width:100%; margin-top:40px; border-top:3px solid #363636; border-spacing:0; border-collapse: collapse; font-size:16px; text-align:left;
	&__th{padding:20px 40px; font-weight:600; border-bottom:1px solid #c7c7c7; box-sizing:border-box;
		&:first-child{border-right:1px solid #c7c7c7;}
	}
	&__th-header{font-weight:600; text-align:center; background-color:#f1f4f9;}
	&__td{padding:20px 40px; border-bottom:1px solid #c7c7c7; box-sizing:border-box; font-weight:500;}
	&__width{width:320px;}
	@media only screen and (max-width:$tablet-size) {margin-top:20px; font-size:13px;
		&__width{width:90px;}
		&__th{padding:15px;}
		&__td{padding:15px;}
	}
}
