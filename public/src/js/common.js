



/*gnb*/
$(function() {
	var gnb = $('#gnb');
	var gnbState = 0;
	$(window).on("load resize", function () {
		if ($('body').attr('data-mobile') == 'false'){
			$(gnb).removeClass('js-open-m').find("li").removeClass('js-open-m');
			$('.responsiveImg').attr('src',function(){return this.src.replace("_m.","_p.")});
			$('.js-dim').removeClass('is-active');
			$('.tab-v1__list').removeClass('js-open-m');
			$('.gnb__depth1-item').on('mouseenter',function(){
				$('.dim').addClass('is-active');
			});
			$(gnb).on('mouseleave',function(){
				$('.dim').removeClass('is-active');
			});
		} else {
			$(gnb).removeClass('js-open-d').find("li").removeClass('js-open-d');
			$('.gnb__depth2-item').removeClass('is-active');
			$('.responsiveImg').attr('src',function(){return this.src.replace("_p.","_m.")});
			// $('.js-dim').addClass('is-active');
		}
	});

	$(gnb.selector+">ul>li>a").on("click mouseenter focus touchstart",function(e) {
		if ($('body').attr('data-mobile') == 'false') {
			$('.gnb__depth1-item').addClass('js-open-d');
			$(gnb).addClass('js-open-d');
			$('.main-search').removeClass('is-active');
		} else {
			if(e.type == "click"){
				e.preventDefault();
				if ($(this).parents("li").hasClass("js-open-m")) {
					$(this).parents("li").removeClass("js-open-m");
				} else {
					$(this).parents("li").siblings().removeClass("js-open-m");
					$(this).parents("li").addClass("js-open-m");
				}
			}
		}
	});
	$(gnb).on("mouseleave",function() {
		$(this).removeClass('js-open-d');
		//$(gnb).find("[class$=tit]").stop().animate({height:'0'},300);
		$(this).find('li').removeClass('js-open-d js-first');
		gnbState = 0;
	});
	$(gnb).find("a").last().on("blur",function() {
		$(gnb).trigger("mouseleave");
	});
	$(gnb).find("[class$=tit]").on("click" ,function(){
		if ($('body').attr('data-mobile') == "false") return false;
		if (!$(gnb).hasClass("js-open-m")) {
			$(gnb).addClass('js-open-m');
		} else {
			$(gnb).removeClass('js-open-m');
		}
		$('.logo, .js-quick').addClass('is-hidden');
	});
	$(gnb).find('.gnb__depth2-link').on("click" ,function(e){
		var gnbData = $(this).attr('data-depth');
		var gnbHeight = $(this).attr('data-height');
		if (gnbData == 'true'){
			e.preventDefault();
			if ($(this).parents(".gnb__depth2-item").hasClass("is-active")) {
				$(this).parents(".gnb__depth2-item").removeClass("is-active");
				$('.gnb__tit').removeClass('is-active');
			} else {
				$(this).parents(".gnb__depth2-item").siblings().removeClass("is-active");
				$(this).parents(".gnb__depth2-item").addClass("is-active");
				if(gnbHeight == 'true'){$('.gnb__tit').addClass('is-active');}
			}
		}
		var gnbClass = $(this).attr('class');
		var gnbClass2 = gnbClass.split(' ');
		if ($('body').attr('data-mobile') == "false") return true;
		if (gnbClass2[1] == "disabled") return true;
		if(e.type == "click"){
			e.preventDefault();
			if ($(this).parents(".gnb__depth2-item").hasClass("js-open-m")) {
				$(this).parents(".gnb__depth2-item").removeClass("js-open-m");
			} else {
				$(this).parents(".gnb__depth2-item").siblings().removeClass("js-open-m");
				$(this).parents(".gnb__depth2-item").addClass("js-open-m");
			}
		}
	});
	$('.gnb__menu-close').on('click',function(e){
		e.preventDefault();
		$(gnb).removeClass('js-open-m');
		$('.logo, .js-quick').removeClass('is-hidden');
	});
	$(window).on("scroll", function(e) {
		if ($('body').attr('data-mobile') == "false") return false;
		var wrap = $(gnb).parent();
		if ($(this).scrollTop() > $(wrap).height()) {
			$(wrap).addClass("js-fixed");
			$("#familySite").removeClass("js-open-m");
		} else {
			$(wrap).removeClass("js-fixed");
		}
	});
});

$(document).ready(function(){
	/*quick*/
	$('.js-quick').on('click',function(e){
		e.preventDefault();
		$('.quick').show();
	});
	$('.js-close').on('click',function(e){
		e.preventDefault();
		$('.quick').hide();
	});
	/*tab*/
	$(window).scroll(function(){
		if($(this).scrollTop()>0){
			$('.tab-v1__list').addClass('is-active');
			$('.js-tab').removeClass('type-white');
		}else{
			$('.tab-v1__list').removeClass('is-active');
			$('.js-tab').addClass('type-white');
		}
	});
});

//tab

$(function() {
	$('.tab-v1').on('click',function(e){
		// e.preventDefault();
		if ($('body').attr('data-mobile') == 'true'){
			if ($(this).find('.tab-v1__list').hasClass("js-open-m")) {
				$(this).find('.tab-v1__list').removeClass("js-open-m");
				$('.tab-v1.type-white').find('.tab-v1__m-tit').removeClass('color8');
				$('tab-v1').removeClass('js-white');
			} else {
				$(this).find('.tab-v1__list').addClass("js-open-m");
				$('.tab-v1.type-white').find('.tab-v1__m-tit').addClass('color8');
				$('tab-v1').addClass('js-white');
			}
			if($(this).parents('.js-dim').hasClass('is-active')){
				$(this).parents('.js-dim').removeClass("is-active");
			} else {
				$(this).parents('.js-dim').addClass("is-active");
			}
		}
	});
});



// $(function() {
// 	var tab = $("[data-tab='true']");
// 	var hash = window.location.hash;
// 	var tt= [];
//
// 	$(tab).find("a").each(function(idx){
// 		var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
// 		tt.push(t);
//
// 		if (hash) {
// 			sele($(tab).find("a[href="+hash+"]"));
// 			for (var i in tt) {
// 				$("#"+tt[i]).removeClass("is-active");
// 			}
// 			$(hash).addClass("is-active");
// 		}
//
// 		$(this).on("click", function(e){
// 			if (this.href.match(/#([^ ]*)/g)) {
// 				e.preventDefault();
// 				if (!$(this).parent().hasClass("is-active")) window.location.hash = ($(this).attr("href"));
// 				sele($(this));
// 				for (var i in tt) {
// 					$("#"+tt[i]).removeClass("is-active");
// 				}
// 				$("#"+t).addClass("is-active");
// 			}
// 		});
//
//
//
// 	})
// 	if ($(tab).hasClass("tab-v1")) {
// 		$(tab).find("[class$=list]").on("click", function(){
// 			//console.log($('body').attr('data-mobile'));
// 			if ($('body').attr('data-mobile') == 'true'){
// 				if ($(this).hasClass("js-open-m")) {
// 					$(this).removeClass("js-open-m");
// 				} else {
// 					$(this).addClass("js-open-m");
// 				}
// 				if($(this).parents('.js-dim').hasClass('is-active')){
// 					$(this).parents('.js-dim').removeClass("is-active");
// 				} else {
// 					$(this).parents('.js-dim').addClass("is-active");
// 				}
// 			}
// 		});
// 		$(window).resize(function(){
// 			if ($('body').attr('data-mobile') == 'false') $(".tab-v1").removeClass("js-open-m");
// 		});
// 	}
//
// 	function sele(el) {
// 		$(el).parent().addClass("is-active").siblings().removeClass("is-active");
// 	}
//
// });
// $(window).on('hashchange', function(){
// 	if (location.hash == "#tab1"){
// 		$('.tab-v1__item').eq(0).addClass('is-active').siblings().removeClass('is-active');
// 		$('#tab1').addClass('is-active').siblings().removeClass('is-active');
// 	} else if(location.hash == "#tab2"){
// 		$('.tab-v1__item').eq(1).addClass('is-active').siblings().removeClass('is-active');
// 		$('#tab2').addClass('is-active').siblings().removeClass('is-active');
// 	}
// });
