$(document).ready(function(){
	$(window).load(function(){
		/*슬라이드*/
		$('.main__visual').bxSlider({
			auto:true,
			autoControls:true,
			pagerSelector:'.page__list-main',
			autoControlsSelector:'.page__start-main',
			autoControlsCombine:true
			,
			onSliderLoad: function(){
				$('.main__slider-tit, .main__slider-txt').addClass('is-active');
			},
			onSlideBefore: function(){
				$('.main__slider-tit, .main__slider-txt').removeClass('is-active');
			},
			onSlideAfter: function(){
				$('.main__slider-tit, .main__slider-txt').addClass('is-active');
			}
		});
		var main2Slider = $('.main2__visual').bxSlider({
			autoControls:true,
			pagerSelector:'.page__list-main2'
		});
		$( '.main2__btn-next' ).on( 'click', function () {
			main2Slider.goToNextSlide();
			return false;
		} );
		$( '.main2__btn-prev' ).on( 'click', function () {
			main2Slider.goToPrevSlide();
			return false;
		} );
		var prodSlider = $('.prod__list').bxSlider({
			autoControls:true,
			pagerSelector:'.page4__list'
		});
		$( '.prod__btn-next' ).on( 'click', function () {
			prodSlider.goToNextSlide();
			return false;
		} );
		$( '.prod__btn-prev' ).on( 'click', function () {
			prodSlider.goToPrevSlide();
			return false;
		} );
		var genieSlider1 = $('.genie__slider1').bxSlider({
			auto:true,
			autoControls:true,
			pagerSelector:'.page__list-prod1',
			autoControlsSelector:'.page__start-prod1',
			autoControlsCombine:true,
			pause:10000,
			onSliderLoad: function(){
				$('.js-slide-1').addClass('is-active');
			},
			onSlideBefore: function(){
				$('.js-slide-1').removeClass('is-active');
			},
			onSlideAfter: function(){
				$('.js-slide-1').addClass('is-active');
			}
		});
		var genieSlider2 = $('.genie__slider2').bxSlider({
			auto:true,
			autoControls:true,
			pagerSelector:'.page__list-prod2',
			autoControlsSelector:'.page__start-prod2',
			autoControlsCombine:true,
			pause:10000,
			onSliderLoad: function(){
				$('.js-slide-2').addClass('is-active');
			},
			onSlideBefore: function(){
				$('.js-slide-2').removeClass('is-active');
			},
			onSlideAfter: function(){
				$('.js-slide-2').addClass('is-active');
			}
		});
	});
	$('.main3__news').cycle({
		fx:'fade',
		swipe:true,
		timeout:0,
	    slides:'.main3__news-item',
		pager:'.page3__list'
	});
	/*탭바*/
	$('.js-bar').on('click',function(e){
		$(this).index();
		e.preventDefault();
		$(this).parents('.main__item').addClass('is-active').siblings().removeClass('is-active');
		if ($('body').attr('data-mobile') == 'false'){
			$('.main__item').css({'padding-top':'0px','height':'70px'});
			$('.main__item.is-active').css({'padding-top':'0px','height':'790px'});
		}else{
			$('.main__item').css({'padding-top':'0%','height':'45px'});
			$('.main__item.is-active').css({'padding-top':'121.5625%','height':'0px'});
		}
		var i = $('.js-bar').index(this);
		if(i == 1){
			$('.js-bar').removeClass('is-active');
			$('.js-bar').eq(0).addClass('is-active');
		}else if(i == 2){
			$('.js-bar').removeClass('is-active');
			$('.js-bar').eq(0).addClass('is-active');
			$('.js-bar').eq(1).addClass('is-active');
		}else if(i == 3){
			$('.js-bar').removeClass('is-active');
			$('.js-bar').eq(0).addClass('is-active');
			$('.js-bar').eq(1).addClass('is-active');
			$('.js-bar').eq(2).addClass('is-active');
		}else{
			$('.js-bar').removeClass('is-active');
		}
		// mainH();
	});
	/*비디오*/
	$('.js-video').on('click',function(e){
		e.preventDefault();
		$('.video').fadeIn('slow',function(){
			$('.video__vertical').addClass('is-active');
		});
	});
	$('.video__close').on('click',function(e){
		e.preventDefault();
		$('.video__vertical').removeClass('is-active',function(){
			$('.video').fadeOut('slow')
		});
	});
});
$(window).on("load resize", function () {
	$('body').attr('data-mobile',
		(function(){
			var r = ($(window).width() <= 1023) ? true : false;
			return r;
		}())
	);
	/*레이아웃잡기*/
	var footerH = $('.footer').outerHeight();
	$('.content, .main').css('padding-bottom',footerH);
	$('.footer').css('margin-top',-footerH);
	if ($('body').attr('data-mobile') == 'false'){
		$('.main__item').css({'padding-top':'0px','height':'70px'});
		$('.main__item.is-active').css({'padding-top':'0px','height':'790px'});
		$('.sns__wrap').on('mouseenter',function(){
			$(this).addClass('is-active');
		}).on('mouseleave',function(){
			$(this).removeClass('is-active');
		});
	} else {
		$('.main__item').css({'padding-top':'0%','height':'45px'});
		$('.main__item.is-active').css({'padding-top':'121.5625%','height':'0px'});
		$('.sns__wrap').off('mouseleave mouseenter');
	}
	// mainH();
	var multiView = ($(window).width() <= 1023) ? 1 : 4 ;
    $('.js-sns').touchSlider({
        roll : true,
        view : multiView,
        resize : true,
		transition: true,
		sidePage : true,
		initComplete : function (e) {
			var _this = this;
			var $this = $(this);
			var paging = $this.next(".paging");
			var len = Math.ceil(this._len / this._view);

			paging.html("");
			for(var i = 1; i <= len; i++) {
				paging.append('<button type="button" class="btn_page">page' + i + '</button>');
			}

			paging.find(".btn_page").bind("click", function (e) {
				_this.go_page($(this).index());
			});
		},
		counter : function (e) {
			$(this).next(".paging").find(".btn_page").removeClass("on").eq(e.current-1).addClass("on");
		}
    });
	var multiView2 = ($(window).width() <= 1023) ? 1 : 3 ;
	$('.js-event').touchSlider({
        roll : true,
        view : multiView2,
        resize : true,
		transition: true,
		sidePage : true,
		initComplete : function (e) {
			var _this = this;
			var $this = $(this);
			var paging = $this.next(".paging");
			var len = Math.ceil(this._len / this._view);

			paging.html("");
			for(var i = 1; i <= len; i++) {
				paging.append('<button type="button" class="btn_page">page' + i + '</button>');
			}

			paging.find(".btn_page").bind("click", function (e) {
				_this.go_page($(this).index());
			});
		},
		counter : function (e) {
			$(this).next(".paging").find(".btn_page").removeClass("on").eq(e.current-1).addClass("on");
		}
    });
}).resize();
